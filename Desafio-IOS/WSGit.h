//
//  WSGit.h
//  Desafio-IOS
//
//  Created by Marco Marques on 26/08/16.
//  Copyright © 2016 Marco Henrique Maia Marques. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Services.h"

@interface WSGit : Services


+(void)getResitoryGit;
+(void)loadingItensWithBlock:(void (^)(NSArray *objects, NSError *error))block;

@end

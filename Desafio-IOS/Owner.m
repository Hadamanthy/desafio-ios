//
//  Owner.m
//  Desafio-IOS
//
//  Created by Marco Marques on 29/08/16.
//  Copyright © 2016 Marco Henrique Maia Marques. All rights reserved.
//

#import "Owner.h"

@implementation Owner

- (NSString *)description
{
    return [NSString stringWithFormat:@"Login= %@, URL AVAGAR: %@", self.login, self.avatar_url];
}

+(Owner *) withDictionary:(NSDictionary *)disctionary {
    Owner *owner = [[Owner alloc] init];
    [owner setLogin:[disctionary objectForKey:@"login"]];
    [owner setAvatar_url:[disctionary objectForKey:@"avatar_url"]];
    
    return owner;
}


@end

//
//  CellRepositoryTableView.m
//  Desafio-IOS
//
//  Created by Marco Marques on 27/08/16.
//  Copyright © 2016 Marco Henrique Maia Marques. All rights reserved.
//

#import "CellRepositoryTableView.h"

@implementation CellRepositoryTableView

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

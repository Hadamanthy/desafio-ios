//
//  Repository.h
//  Desafio-IOS
//
//  Created by Marco Marques on 28/08/16.
//  Copyright © 2016 Marco Henrique Maia Marques. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Owner.h"

@interface Repository : NSObject


@property (strong) NSString *nameRepository;
@property (strong) NSString *descRepository;
@property (strong) NSString *userAuthor;
@property (strong) NSString *nameAuthor;
@property (strong) NSString *photoAuthor;
@property (strong) NSString *numberStar;
@property (strong) NSString *numberForks;

@property (strong) NSString *urlForks;
@property (strong) Owner *owner;

+(Repository *) withDictionary: (NSDictionary *)disctionary;

@end

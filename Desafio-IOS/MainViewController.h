//
//  ViewController.h
//  Desafio-IOS
//
//  Created by Marco Marques on 26/08/16.
//  Copyright © 2016 Marco Henrique Maia Marques. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>


@property (weak, nonatomic) IBOutlet UIBarButtonItem *sideBarButton;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSArray *itens;

@end


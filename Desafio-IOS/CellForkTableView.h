//
//  CellForkTableView.h
//  Desafio-IOS
//
//  Created by Marco Marques on 29/08/16.
//  Copyright © 2016 Marco Henrique Maia Marques. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellForkTableView : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameFork;
@property (weak, nonatomic) IBOutlet UILabel *descFork;
@property (weak, nonatomic) IBOutlet UIImageView *photoUser;
@property (weak, nonatomic) IBOutlet UILabel *username;
@property (weak, nonatomic) IBOutlet UILabel *nameFull;
@end

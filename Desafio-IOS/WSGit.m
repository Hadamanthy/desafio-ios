//
//  WSGit.m
//  Desafio-IOS
//
//  Created by Marco Marques on 26/08/16.
//  Copyright © 2016 Marco Henrique Maia Marques. All rights reserved.
//

#import "WSGit.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "AFHTTPRequestOperationManager.h"
#import "Repository.h"


NSString *const BASE_URL_GIT = @"https://api.github.com/search/repositories?q=language:Java&sort=stars&page=1";



@implementation WSGit


+(void)loadingItensWithBlock:(void (^)(NSArray *objects, NSError *error))block {
    
    NSURL *baseUrl = [NSURL URLWithString:BASE_URL_GIT];
    
    [self loadItemsFromUrl:baseUrl withBlock:^(NSArray *objects, NSError *error) {
        if (error) {
            NSLog(@"load itens erro %@",error.description);
            block(nil, error);
            return;
        }
        
        block(objects,nil);
    }];
}


+(void)getResitoryGit {
    
    NSURL *baseUrl = [NSURL URLWithString:BASE_URL_GIT];
    NSURLRequest *request = [NSURLRequest requestWithURL:baseUrl];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *jsonDict = (NSDictionary *) responseObject;
        NSArray *repositoryArray = [jsonDict objectForKey:@"items"];
        
       
    
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error Retrieving Weather"
                                                            message:[error localizedDescription]
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertView show];
    }];
    
    [operation start];
    
    
}
@end

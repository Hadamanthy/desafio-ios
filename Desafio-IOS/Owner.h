//
//  Owner.h
//  Desafio-IOS
//
//  Created by Marco Marques on 29/08/16.
//  Copyright © 2016 Marco Henrique Maia Marques. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Owner : NSObject

@property (strong) NSString *avatar_url;
@property (strong) NSString *login;

+(Owner *) withDictionary:(NSDictionary *)disctionary;


@end

//
//  ForkViewController.m
//  Desafio-IOS
//
//  Created by Marco Marques on 29/08/16.
//  Copyright © 2016 Marco Henrique Maia Marques. All rights reserved.
//

#import "ForkViewController.h"
#import "CellForkTableView.h"
#import "SWRevealViewController.h"

@interface ForkViewController ()

@end

@implementation ForkViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Forks";
    SWRevealViewController *revealViewController = self.revealViewController;
    
    
    if(revealViewController){
        
        [self.sideBarButton setTarget:self.revealViewController];
        [self.sideBarButton setAction:@selector(revealToggle:)];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 20;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath: (NSIndexPath *)indexPath {
    NSLog(@"TESte");
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CellForkTableView *cellFork = [tableView dequeueReusableCellWithIdentifier:@"forkViewController"];
    
    if(!cellFork) {
        [tableView registerNib:[UINib nibWithNibName:@"CellForkTableView" bundle:nil] forCellReuseIdentifier:@"forkViewController"];
        cellFork = [tableView dequeueReusableCellWithIdentifier:@"forkViewController"];
    }
    
    
    return cellFork;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

@end

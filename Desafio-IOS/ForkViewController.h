//
//  ForkViewController.h
//  Desafio-IOS
//
//  Created by Marco Marques on 29/08/16.
//  Copyright © 2016 Marco Henrique Maia Marques. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForkViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>


@property (weak, nonatomic) IBOutlet UIBarButtonItem *sideBarButton;

@property (weak, nonatomic) IBOutlet UITableView *tableView;


@end

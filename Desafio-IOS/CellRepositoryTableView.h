//
//  CellRepositoryTableView.h
//  Desafio-IOS
//
//  Created by Marco Marques on 27/08/16.
//  Copyright © 2016 Marco Henrique Maia Marques. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellRepositoryTableView : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameRepository;

@property (weak, nonatomic) IBOutlet UILabel *descRepository;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *nameFull;
@property (weak, nonatomic) IBOutlet UILabel *numberStars;
@property (weak, nonatomic) IBOutlet UILabel *numberForks;


@property (weak, nonatomic) IBOutlet UIImageView *photoProfile;
@end

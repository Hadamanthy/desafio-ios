//
//  Services.m
//  Desafio-IOS
//
//  Created by Marco Marques on 28/08/16.
//  Copyright © 2016 Marco Henrique Maia Marques. All rights reserved.
//

#import "Services.h"
#import "AFHTTPRequestOperation.h"
#import "AFNetworking.h"

double const TIME_OUT = 20.0;

@implementation Services


+ (void)loadItemsFromUrl:(NSURL *)url withBlock:(void (^)(NSArray *objects, NSError *error))block {
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:TIME_OUT];
    request.timeoutInterval = TIME_OUT;
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSDictionary *jsonDict = (NSDictionary *) responseObject;
         NSArray *repository = [jsonDict objectForKey:@"items"];
        
         block(repository, nil);
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         block(nil, error);
     }];
    
    [operation start];
}


+ (void)loadItemsFromUrl:(NSURL *)url {
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:TIME_OUT];
    //request.timeoutInterval = TIME_OUT;
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSDictionary *jsonDict = (NSDictionary *) responseObject;
         NSArray *repositoryArray = [jsonDict objectForKey:@"items"];
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
        
     
     
     }];
    
    [operation start];
}

@end

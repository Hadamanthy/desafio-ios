//
//  ViewController.m
//  Desafio-IOS
//
//  Created by Marco Marques on 26/08/16.
//  Copyright © 2016 Marco Henrique Maia Marques. All rights reserved.
//

#import "MainViewController.h"
#import "SWRevealViewController.h"
#import "CellRepositoryTableView.h"
#import "WSGit.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "AFHTTPRequestOperationManager.h"
#import "Repository.h"
#import "Owner.h"
#import "Services.h"
#import "ForkViewController.h"


NSString *const BASE_URL = @"https://api.github.com/search/repositories?q=language:Java&sort=stars&page=1";



@interface MainViewController ()




@end

@implementation MainViewController {
    NSArray *tableData;
    
}


- (id)init {
    self = [super init];
    
    if(self){
       
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"GitHub JavaPop";

    [self loadItens];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    SWRevealViewController *revealViewController = self.revealViewController;
  
    
    if(revealViewController){
        
        [self.sideBarButton setTarget:self.revealViewController];
        [self.sideBarButton setAction:@selector(revealToggle:)];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
}

-(void) viewWillAppear:(BOOL)animated
{
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_itens count];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath: (NSIndexPath *)indexPath {
    [self exibeTelaFork];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CellRepositoryTableView *cellRepository = [tableView dequeueReusableCellWithIdentifier:@"repositoryCell"];
    
    if(!cellRepository) {
        [tableView registerNib:[UINib nibWithNibName:@"CellRepositoryTableView" bundle:nil] forCellReuseIdentifier:@"repositoryCell"];
        cellRepository = [tableView dequeueReusableCellWithIdentifier:@"repositoryCell"];
    }
    
        Repository *repo = [self montaObjeto: indexPath.row];
    
        cellRepository.nameRepository.text = repo.nameRepository;
        cellRepository.descRepository.text = repo.descRepository;
        cellRepository.numberForks.text = [NSString stringWithFormat: @"%d", repo.numberForks];
        cellRepository.numberStars.text = [NSString stringWithFormat: @"%d", repo.numberStar];;
       
    
        [cellRepository.photoProfile setImage:[UIImage imageNamed:@"photoProfile.png"]];
        
    
    return cellRepository;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

-(Repository *)montaObjeto: (long)index{
    
    Repository *repository = [[Repository alloc] init];
    
    repository.nameRepository = [[_itens objectAtIndex: index] objectForKey:@"name"] ? [[_itens objectAtIndex: index] objectForKey:@"name"] : @"Nome não localizado";
    repository.descRepository = [[_itens objectAtIndex:index] objectForKey:@"full_name"] ? [[_itens objectAtIndex:index] objectForKey:@"full_name"] : @"Não localizado";
        
    repository.numberForks = [[_itens objectAtIndex:index] objectForKey:@"forks_count"];
    repository.numberStar = [[_itens objectAtIndex:index] objectForKey:@"stargazers_count"];

    NSArray *ownerArray = [_itens objectAtIndex:index];
    
    NSString *url_avatar = [ownerArray lastObject];
    
    
    return repository;
}


-(void) exibeTelaFork {
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ForkViewController *forkViewController = [storyBoard instantiateViewControllerWithIdentifier:@"forkViewController"];
    [self.navigationController pushViewController:forkViewController animated:YES];
    
}

-(void)loadItens {
    
    [WSGit loadingItensWithBlock:^(NSArray *objects, NSError *error) {
        if (error) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Não foi possível encontrar pontos." delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
            [alert show];
            
            return;
        }
        _itens = objects;
        [self.tableView reloadData];
        
        //NSLog(@"%@", (NSDictionary *)objects);
        
    }];
    
}

@end

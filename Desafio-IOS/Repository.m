//
//  Repository.m
//  Desafio-IOS
//
//  Created by Marco Marques on 28/08/16.
//  Copyright © 2016 Marco Henrique Maia Marques. All rights reserved.
//

#import "Repository.h"

@implementation Repository


- (NSString *)description
{
    return [NSString stringWithFormat:@"Nome repository: %@, Descricao Repository: %@, Numero de Forks: %@, Numero de Star: %@, USuario: %@",
                self.nameRepository, self.descRepository, self.numberForks, self.numberStar, self.userAuthor];
}


+(Repository *) withDictionary:(NSDictionary *)disctionary {
    Repository *repository = [[Repository alloc] init];
    [repository setNameAuthor:[disctionary objectForKey:@"name"]];
    [repository setDescRepository:[disctionary objectForKey:@"full_name"]];
    [repository setNumberStar:[disctionary objectForKey:@"forks_count"]];
    [repository setNumberForks:[disctionary objectForKey:@"stargazers_count"]];
    
    Owner *owner = [Owner withDictionary:[disctionary objectForKey:@"owner"]];
    
    [repository setOwner:owner];
    return repository;
}

@end

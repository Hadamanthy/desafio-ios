//
//  Services.h
//  Desafio-IOS
//
//  Created by Marco Marques on 28/08/16.
//  Copyright © 2016 Marco Henrique Maia Marques. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Services : NSObject

+ (void)loadItemsFromUrl:(NSURL *)url withBlock:(void (^)(NSArray *objects, NSError *error))block;
+ (void)loadItemsFromUrl:(NSURL *)url;

@end
